import { Component } from "@angular/core";




@Component({
    selector: 'app-heroe',
    templateUrl: 'heroe.component.html'
})
export class HeroeComponent {
    nombre: string = 'Jose Luis';
    edad: number = 28;

    get obtenerNombre(){
        return this.nombre + ' - ' + this.edad;
    }

    capitalizado(): string{
        return this.nombre.toUpperCase();
    }

    cambiarNombre(): any{
        this.nombre = 'Goku';
    }

    cambiarEdad(): any {
        this.edad = 22;
    }


}